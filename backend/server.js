import express from 'express';
import connectMongoDB from './lib/mongoose.js'
import morgan from 'morgan';
import cors from 'cors';
import dotenv from 'dotenv';
import userRoutes from './routes/user.js';

const app = express();
const hostname = '127.0.0.1';
const port = process.env.PORT || 9090;

dotenv.config();

app.use(cors({
  origin: 'http://localhost:5173',
  methods: ['GET', 'POST','PUT','DELETE'], 
  allowedHeaders: ['Content-Type', 'Authorization'], 
  credentials: true 
}));
app.use(morgan('dev'));
app.use(express.json());

app.use('/user', userRoutes);

//app.use(notFoundError);
//app.use(errorHandler);

connectMongoDB()
  .then(() => {
    app.listen(port, hostname, () => {
      console.log(`Server running at http://${port}`);
    });
  })
  .catch((error) => {
    console.error("Error connecting to MongoDB:", error);
  });