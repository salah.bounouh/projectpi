import backgroundImg from '../assets/backgroudImg.jpg';
import {Outlet} from 'react-router-dom';
import { useLocation } from 'react-router-dom';
import { useResponsive } from '../hooks/useResponsive';
import doctorVideo from '../assets/doctorVideo.jpg'
import doctorVR from '../assets/doctorVR.jpg'
import ServiceCard from '../components/serviceCard'
import ImgCard from '../components/imgCard'
import IconLinkCard from '../components/iconLinkCard'
import serviceData from '../constants/serviceData'
import imgCardData from '../constants/imgCardData'
import iconCardData from '../constants/iconCardData'
import VrCard from '../components/vrCard'
import QuestionAccordion from '../components/questionAccordion';


export default function Landing(){
  const location = useLocation();
  const { pathname } = location;

  const isRootPath = pathname === "/";

  const isMobile = useResponsive();

  return (
    <div 
      style={{
        display:'flex',
        justifyContent:'center',
        alignItems:'center',
        width:'100%',
        height:`${isMobile? '100%' : '100%'}`,
        backgroundImage: `url(${backgroundImg})`,
        backgroundSize: 'cover',
        backgroundPosition: 'center',
      }}>

      <div style={{
        display:'flex',
        marginTop:'80px',
        width:`${isMobile? '100%' : '90%' }`,
        height:'90%',
        alignItems:'center',
        justifyContent:'center',
        backgroundColor:'rgb(255,255,255,0.9)',
        border:'2px solid gray',
        borderRadius:'10px'
      }}
      >  
      {!isRootPath && 
        <Outlet/>
        }
      { isRootPath &&
        <div style={{
          display:"flex",
          padding:'5px',
          width:"100%",
          alignItems:'center',
          flexDirection:'column',
        }}>
          <h1 className='text-4xl font-mono font-bold text-blue-950 py-12'>Welcome To Our Online Medical Clinic</h1>
          <div style={{
            display:'flex',
            flexDirection: `${isMobile? 'column': 'row'}`
          }}>
            <div className='flex flex-col p-8'>
              <span className="bg-blue-900 text-blue-200 text-s w-14 font-medium me-2 px-2.5 py-0.5 rounded dark:bg-blue-900 dark:text-blue-300">60dt</span>
              <h1 className='text-4xl font-mono text-blue-900'>
                Book your <span> video </span> 
                <br/><span className='text-5xl font-bold font-mono'>consultation </span>today
                <br/>Or try our <span className='text-5xl font-bold font-mono'>VR application</span> 
              </h1>
              <p className='mt-5 text-gray-800 font-mono font-bold'>Online medical consultations and electronic prescription services <br/> offer a convenient way for individuals to access healthcare services <br/>  without having to travel to a doctor’s office or clinic. <br/>  </p>
              <p className='mt-5 text-gray-800 font-mono font-bold'> Try out our VR application for more realistic consultation. </p>
            </div>
            <div style={{ 
              display:'flex',
              height:'600px',
              width:'450px',
              padding:'10px',
              backgroundColor:'rgb(225,225,225,1)',
              borderRadius:'100px'
            }}>
              <img src={doctorVideo} className='w-full h-full '/>
            </div> 
          </div>  
          <div
            style={{
              display:'flex',
              flexDirection:'column',
              justifyContent:'center',
              alignItems:'center',
              marginTop:'100px'
            }}
          >
            <h1 className='text-4xl mb-10 font-mono font-bold text-blue-900'> Popular Services</h1>
            <div style={{
              display:'grid',
              gridTemplateColumns:`${isMobile? '100%': '33% 33% 33%'}`,
              gap:'10px'
              }}>
              {serviceData.map((service, index) => (
                <ServiceCard key={index} {...service} />
              ))}
            </div>
          </div> 
          <div style={{
            display:'flex',
            flexDirection:'column',
            marginTop:'120px',
            justifyContent:'center',
            alignItems:'center',
            paddingInline:'210px'
          }}>
            <h1 className='text-4xl mb-10 font-mono font-bold text-blue-900'> Main Categories</h1>
            <p className='mt-5 text-gray-800 font-mono font-bold'>
              We offer patients a range of different specialist appointments conducted via our GPs.
              Each section enables users to book either a video consultation or request specific medications without the need to see a clinician.
              Please choose your preferred consultation type from one of the categories below:
            </p>
          </div>

          <div
            style={{
              display:'flex',
              flexDirection:`${isMobile? 'column' : 'row'}`,
              marginTop:'50px', 
              gap:`${isMobile? '250px' : '20px'}`
            }}
          >
            {imgCardData.map((data, index) => (
                <ImgCard key={index} {...data} />
            ))}
          </div>
          <div
            style={{
              display:'flex',
              flexDirection:'column',
              marginTop:'250px',
              width:'100%',
              alignItems:'center',
              backgroundColor:'rgb(211, 248, 250, 0.5)',
              gap:'20px'
            }}
          >
            <h1 className='text-4xl font-bold mb-10 font-mono  text-blue-900'> Get Medical Care In 
              <span className='text-5xl font-extrabold'> Three Easy Steps</span>
            </h1>
            <div
             style={{
               display:'flex',
               flexDirection:`${isMobile? 'column': 'row'}`,
               marginBottom:'40px',
               gap:'10%'
               
             }}>
              {iconCardData.map((data, index) => (
                <IconLinkCard key={index} {...data} />
              ))}
            </div>
            <VrCard img={doctorVR}/>
          </div>
          <div 
           style={{
            display:'flex',
            flexDirection:'column',
            marginTop:'90px',
            justifyContent:'center',
            alignItems:'center',
            gap:'30px'
           }}  
          >
            <h1 className='text-4xl mb-10 font-mono font-bold text-blue-900'>Answers To Frequently Asked Questions</h1>
            <QuestionAccordion/>
          </div>
        </div>
      } 
      </div>
    </div>
  )
}
