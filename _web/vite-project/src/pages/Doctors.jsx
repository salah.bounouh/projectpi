import DoctorCard from '../components/doctorCard'
import img1 from '../assets/img1.jpg'


export default function Doctors() {
  return (
    <div
      style={{
        display:"flex",
        flexDirection:"column",
        gap:"40px",
        justifyContent:"center",
        alignItems:'center'
      }}
    >
      <h1 className='text-5xl font-mono font-extrabold text-blue-950 mt-8'>Doctors list</h1>
      <h1 className='text-4xl font-mono font-bold text-blue-950 mb-6'>Select the Doctor and book for an appointment</h1>
      <div 
        style={{
          display:'flex',
          gap:'20px'
        }}
      >
        <DoctorCard
          picture={img1}
          fullName="Dr Salah"
          specialization="Generalist"
        />
        <DoctorCard
          picture={img1}
          fullName="Dr Salah"
          specialization="Generalist"
        />
        <DoctorCard
          picture={img1}
          fullName="Dr Salah"
          specialization="Generalist"
        />
      </div>
    </div>
  )
}
