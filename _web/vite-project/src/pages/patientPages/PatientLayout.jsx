import {Outlet} from 'react-router-dom';

const PatientLayout = () => {
    <main className='app'>
        <Outlet/>
    </main>
}
export default PatientLayout;