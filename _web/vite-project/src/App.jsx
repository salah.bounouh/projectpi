import {BrowserRouter, Routes, Route} from 'react-router-dom';
import Login from './pages/Login';
import Landing from './pages/Landing';
import Register from './pages/Register';
import Layout from './pages/doctorPages/doctorLayout';
import RequireAuth from './components/RequireAuth';
import NavBar from './components/navbar'
import About from './pages/About'
import Services from './pages/Services'
import Doctors from './pages/Doctors'
import RequestAppointment from './pages/RequestAppointment'



export default function App(){
  return(
    <>
      <BrowserRouter>
        <div className='fixed w-full'>
          <NavBar/>
        </div>
        <Routes>
          {/* public routes */}
            <Route path="/" element={<Landing/>}>
              <Route path="register" element={<Register/>}/>
              <Route path="login" element={<Login/>}/>
              <Route path="About" element={<About/>} />
              <Route path="Doctors" element={<Doctors/>} />
              <Route path="Services" element={<Services/>} />
              <Route path="request_appointment" element={<RequestAppointment/>}/>
            </Route>
          {/* require access routes */}
          <Route element={<RequireAuth allowedRoles={["doctor", "patient"]}/>}>
            <Route path='layout'  element={<Layout/>}/>
          </Route>
          
          <Route element={<RequireAuth allowedRoles={["doctor"]}/>}>
            <Route path='HomeP' />
          </Route>
            {/* <Route path="homeP" element={<RequireAuth allowedRoles={[ROLES.patient]}><HomeP/></RequireAuth>}/>
          </Route> */}
        </Routes>
      </BrowserRouter >
    </>
  )
}