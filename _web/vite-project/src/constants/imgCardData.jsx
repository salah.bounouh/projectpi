import doctorVRImg from '../assets/doctorVR.jpg'
import doctorVideo from '../assets/doctorVideo2.jpg'

export const imgCardData = [
    {
        link: "/",
        img: doctorVRImg,
        title: 'VR Consultation',
        text: 'Book Now For 60dt'
    },
    {
        link: "/",
        img: doctorVideo,
        title: 'Video Consultation',
        text: 'Book Now For 60dt'
    }
  ]
  export default imgCardData;